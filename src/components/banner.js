function Banner() {
  return (
    <>
      <section className="banner">
        <div className="banner-inner">
          <img src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571172/rocket_design_k4nzbm.png" />
        </div>
      </section>
    </>
  );
}
export default Banner;
