function Cards() {
  var shortcuts = [
    {
      head: "Google Search",
      text: "Also included with the Template is the Template Customization Guide with five special video lessons showing you how to get professional website pictures & how to edit them to fit the template,flessons showing you how to get professional website pictures & how to edit them to fit the template,f",
      id: "google",
    },
    {
      head: "Marketing",
      text: "Note: this template includes a page with a PHP website contact form and requires a web host or a program such as XAMPP to run PHP and display it’s content.id='desktop'lessons showing you how to get professional website pictures & how to edit them to fit the template,f ",
      id: "marketing",
    },
    {
      head: "Happy Customers",
      text: "When you purchase and download The Rocket Design HTML Template you get a full five page responsive HTML website template with both a “light” and “dark” version of the template in addition to the following features:",
      id: "customers",
    },
  ];
  return (
    <>
      <section className="inner-wrapper-3">
        {shortcuts.map((carddata) => {
          return (
            <Card head={carddata.head} text={carddata.text} id={carddata.id} />
          );
        })}

        <section id="smelly">
          <h2>: )</h2>
        </section>
      </section>
    </>
  );
  function Card(props) {
    return (
      <>  
          <section className="one-third" id={props.id}>
            <h3>{props.head}</h3>
            <p>{props.text}</p>
          </section>
      </>
    );
  }
}

export default Cards;
