function Navbar() {
  return (
    <>
      <header>
        <div id="header-inner">
          <a href="#" id="logo"></a>
          <nav>
            <a href="#" id="menu-icon"></a>
            <ul>
              <li>
                <a href="#" className="current">
                  Home
                </a>
              </li>
              <li>
                <a href="#">Skills</a>
              </li>
              <li>
                <a href="#">Portfolio</a>
              </li>
              <li>
                <a href="#">Our team</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>{" "}
    </>
  );
}

export default Navbar;
