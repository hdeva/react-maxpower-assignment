function Socio() {
  var icons = [
    { name: "fa fa-facebook" },
    { name: "fa fa-google-plus" },
    { name: "fa fa-twitter" },
    { name: "fa fa-youtube" },
    { name: "fa fa-instagram" },
  ];
  return (
    <>
      <footer>
        <ul className="social">
          {icons.map((socioimg) => {
            return <Iconsimg img={socioimg.name}  />;
          })}
        </ul>
      </footer>
    </>
  );
  function Iconsimg(props) {
    return (
      <>
        <li>
          <a href="#" target="_blank">
            <i className={props.img}></i>
          </a>
        </li>
      </>
    );
  }
}

export default Socio;
