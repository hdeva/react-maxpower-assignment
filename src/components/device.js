function Devices() {
  var devs = [
    {
      class: "inner-wrapper",
      id1: "tablet",
      imgLink:
        "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_ipad_vr0pfu.png",
      id2: "tablet2",
      head: "Mobile.Tablet.Desktop.",
      text: " A clean HTML layout and CSS stylesheet making for a great responsive framework to design around that includes a responsive drop down navigation menu, image slider, contact form and ‘scroll to the top’ jQuery plugin.jQuery plugin.that includes a responsive drop down navigation menu, image slider, contact form and ‘scroll to the top’ jQuery plugin.",
    },
    {
      class: "inner-wrapper-2",

      id1: "mobile",
      imgLink:
        "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_mobile_stt5sh.png",
      id2: "hand-mobile",
      head: "Accross Each Device",
      text: "A clean HTML layout and CSS stylesheet making for a great responsive framework to design around that includes a responsive drop down navigation menu, image slider, contact form and ‘scroll to the top’ jQuery plugin.that includes a responsive drop down navigation menu, image slider, contact form and ‘scroll to the top’ jQuery plugin.",
    },
    {
      class: "inner-wrapper",
      id1: "desktop1",
      imgLink:
        "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571256/desktop_fsksnx.png",
      id2: "desktop",
      head: "Accross Each Device",
      text: " Also included with the Template is the Template Customization Guide with five special video lessons showing you how to get professional website pictures & how to edit them to fit the template, how to make a custom website logo, how to upload your HTML website template to the internet and an HTML website building tools video!",
    },
  ];
  return (
    <>
      {devs.map((dev) => {
        if(dev.class==="inner-wrapper"){return (
            
            <DeviceLeft 
              class={dev.class}
              imgLink={dev.imgLink}
              head={dev.head}
              text={dev.text}
              id1={dev.id1}
              id2={dev.id2}
            />
          );}
          else{
            return(
                <DeviceRight
              class={dev.class}
              imgLink={dev.imgLink}
              head={dev.head}
              text={dev.text}
              id1={dev.id1}
              id2={dev.id2}
            />
            )
          }
        
      })}
      {/* <section className="inner-wrapper">
        <article id="tablet">
          <img src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_ipad_vr0pfu.png" />
        </article>
        <aside id="tablet2">
          <h2>Mobile.Tablet.Desktop.</h2>
          <p>
           
          </p>
        </aside>
      </section>
      <section className="inner-wrapper-2">
        <article id="mobile">
          <h2>Accross Each Device</h2>
          <p>
            
          </p>
        </article>
        <aside className="hand-mobile">
          <img src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_mobile_stt5sh.png" />
        </aside>
      </section>
      <section className="inner-wrapper">
        <article>
          <img src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571256/desktop_fsksnx.png" />
        </article>
        <aside id="desktop">
          <h2>Desktop</h2>
          <p>
           
          </p>
        </aside>
      </section> */}
    </>
  );
  function DeviceLeft(props) {
    return (
      <>
        <section className={props.class}>
          <article id={props.id1}>
            <img src={props.imgLink} />
          </article>
          <aside id={props.id2}>
            <h2>{props.head}</h2>
            <p>{props.text}</p>
          </aside>
        </section>
      </>
    );
  }
  function DeviceRight(props) {
    return (
      <>
        <section className={props.class}>
          <article id={props.id1}>
            <h2>{props.head}</h2>
            <p>{props.text}</p>
          </article>
          <aside id={props.id2}>
            <img src={props.imgLink} />
          </aside>
        </section>
      </>
    );
  }
}
export default Devices;
