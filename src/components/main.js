import "./main.css";
import Navbar from "./navbar";
import Banner from "./banner";
import FourCol from "./fourcol";
import Cards from "./cards";
import Socio from "./socio";
import Copy from "./copyrights";
import Devices from "./device";
function Main() {
  return (
    <>
      <Navbar />
      <Banner />
      <FourCol />
      <Devices/>
      <Cards />
      <Socio />
      <Copy/>

      
    </>
  );
}

export default Main;
