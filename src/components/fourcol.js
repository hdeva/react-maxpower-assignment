function FourCol() {
  return (
    <>
      <Col icon="fa fa-html5" id="html" name="HTML" />
      <Col icon="fa fa-css3" id="css" name="CSS" />
      <Col icon="fa fa-search" id="seo" name="SEO" />
      <Col icon="fa fa-users" id="social" name="Social" />
    </>
  );
  
}

function Col(props) {
  return (
    <>
      <section className="one-fourth" id={props.id}>
        <td>
          <i className={props.icon}></i>
        </td>
        <h3>{props.name}</h3>
      </section>
    </>
  );
}


export default FourCol;
